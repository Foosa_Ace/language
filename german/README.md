## 1 ##

simple past in German (called Präteritum/Imperfect or unvollendete Vergangenheit) 

for essen (note that it is an irregular verb) would be aßen. Again this needs to be conjugated: "ich aß"

If you would actually say that people would probably look a bit startled, because simple past is rarely used in spoken language nowadays. In contrast to English there is no real difference in usage between simple past and present perfect in German (maybe there was one long time ago) so you can use both interchangeably and most of the time present perfect is used (that is probably the cause why we struggle very hard with English tenses).
Present perfect (called Perfekt or vollendete Vergangenheit) is constructed similarly as in English. You need either a conjugated form of to be (sein) or to have (haben), which depends on the verb, but most of the time it is to have, and the participle (they usually start with ge-). For essen it would be gegessen and the sentence would look like:

Ich habe gegessen
Du hast gegessen.
Er hat gegessen. and so on

There is one big difference to English though. In a main clause the finite verb (that means the conjugated verb) is always at the second place in a sentence. All infinite verbs, i.e. the ones which don't change like the participle, move to the end of the sentence. In the examples above you can not see it, but if you use an object it looks like this:
Ich habe einen Apfel gegessen -- (I have an apple eaten)
Du hast das Brot gegessen -- (You have the bread eaten)

## "You are eating the apple." ##

Actually the key is to look at the form of the verb "essen" in this case. This form is typically used with plurals and that is how I eliminated Du from the choice (Du = singular)
"Sie" can be a formal form of "you". However, an important difference to note for that use case is that it will always be capitalized. Conversely, "sie" (referring to "she" or "they") would normally only be capitalized at the start of a sentence.