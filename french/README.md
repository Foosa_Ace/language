What is the difference between à and en ? There are 5 propositions corresponding to "to" in English. Let's say the sentence starts with " I am going to..."

(chez) + name of a person => Je vais chez mon copain. Je vais chez elle. Je vais chez le médecin etc.

(au, à l', à la) + name of the place => Je vais au bureau, à l'hopital, à la maison etc. Which one to use? Depends on the noun. If it is feminine (à la), if it is masculin (au), if it starts with a vowel (à l')

(à) + name of a city => Je vais à Paris.

(en, au, aux) + name of the country => Je vais en France. Je vais au Maroc. Je vais aux Etats-Unis Which one to use? Again depends on the noun. If it is feminine (en), if it is masculin (au), if it is plural (aux)

There are some very rare exceptions. I dont want to confuse you with them.